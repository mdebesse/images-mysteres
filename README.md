# Images Mystères

[Site disponible ici](https://profdecm.fr/h5p/mathematiques/ImageMystère/)

Pour travailler le calcul mental ou posé de manière ludique, voici quelques activités d'Images Mystères.

Le principe est simple : il s'agit de répondre à 20 questions (calculs ou autres) pour dévoiler au fur et à mesure l'image mystère, chaque bonne réponse dévoilant une partie de l'image.

Je souhaitais à la base réaliser des feuilles de calculs avec un tableur, où chaque bonne réponse dévoile une partie d'un pixel art, mais ayant quelques difficultés avec les formules à utiliser et l'aspect de l'activité. J'ai fini par coder rapidement l'activité sous forme de site internet puis j'ai demandé à Chat GPT d'améliorer mon code (ce qui n'était pas du luxe).

## Autres logiciels utilisés

- [Bootstrap](https://getbootstrap.com/) pour l'interface et certaines animations